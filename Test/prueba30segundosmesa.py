#!/usr/bin/env python
# -*- coding: utf-8 -*-

_INSTALL_PATH = '/usr/share/sugar/activities/TurtleArt.activity'
_ALTERNATIVE_INSTALL_PATH = \
    '/usr/local/share/sugar/activities/TurtleArt.activity'

import os, sys, dbus
paths = []

paths.append('../%s.activity')
paths.append(os.path.expanduser('~') + '/Activities/%s.activity')
paths.append('/usr/share/sugar/activities/%s.activity')
paths.append('/usr/local/share/sugar/activities/%s.activity')
paths.append(
    '/home/broot/sugar-build/build/install/share/sugar/activities/%s.activity')
paths.append('/usr/share/sugar/activities/TurtleBots.activity')
flag = False
for path in paths:
    for activity in ['TurtleBots', 'TurtleBlocks']:
        p = (path % activity) if "%" in path else path

        if os.path.exists(p) and p not in sys.path:
            flag = True
            sys.path.insert(0, p)

if not flag:
    print 'This code require the Turtle Blocks/Bots activity to be installed.'
    exit(1)

from time import *
from random import uniform
from math import *

from pyexported.window_setup import *


tw = get_tw()

BOX = {}
ACTION = {}

global_objects = None
turtles = None
canvas = None
logo = None

def start():
    tw.start_plugins()
    global global_objects,turtles,canvas,logo
    global_objects = tw.get_global_objects()
    turtles = tw.turtles
    canvas = tw.canvas
    logo = tw.lc
    logo.boxes = BOX
    global butia
    butia = global_objects['Butia']
    turtle = turtles.get_active_turtle()
    while True:
        if ((butia.getDistance('4', '0') < 41500.0) and (butia.getDistance('1', '0') < 41500.0)):
            butia.speed(200.0)
            butia.forward()
        else:
            if ((butia.getDistance('4', '0') > 41500.0) and (butia.getDistance('1', '0') > 41500.0)):
                butia.speed(1000.0)
                butia.backward()
                butia.backward()
                butia.backward()
            if (butia.getDistance('1', '0') > 41500.0):
                butia.stop(True)
                butia.speed(600.0)
                butia.backward()
                butia.backward()
                butia.left()
                sleep(1.0)
                yield True
                butia.backward()
            if (butia.getDistance('4', '0') > 41500.0):
                butia.stop(True)
                butia.speed(600.0)
                butia.backward()
                butia.backward()
                butia.right()
                sleep(1.0)
                yield True
                butia.backward()
        yield True
    yield True
ACTION["start"] = start


if __name__ == '__main__':
    tw.lc.start_time = time()
    tw.lc.icall(start)
    gobject.idle_add(tw.lc.doevalstep)
    gtk.main()
