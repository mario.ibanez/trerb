#!/usr/bin/env python
# -*- coding: utf-8 -*-

_INSTALL_PATH = '/usr/share/sugar/activities/TurtleArt.activity'
_ALTERNATIVE_INSTALL_PATH = \
    '/usr/local/share/sugar/activities/TurtleArt.activity'

import os, sys, dbus
from matplotlib import pyplot as plt
import math
import numpy as np
sys.path.insert(0, '/usr/share/sugar/activities/TurtleBots.activity/plugins/butia')

paths = []

paths.append('../%s.activity')
paths.append(os.path.expanduser('~') + '/Activities/%s.activity')
paths.append('/usr/share/sugar/activities/%s.activity')
paths.append('/usr/local/share/sugar/activities/%s.activity')
paths.append(
    '/home/broot/sugar-build/build/install/share/sugar/activities/%s.activity')
paths.append('/usr/share/sugar/activities/TurtleBots.activity')
flag = False
for path in paths:
    for activity in ['TurtleBots', 'TurtleBlocks']:
        p = (path % activity) if "%" in path else path

        if os.path.exists(p) and p not in sys.path:
            flag = True
            sys.path.insert(0, p)

if not flag:
    print ('This code require the Turtle Blocks/Bots activity to be installed')
    exit(1)

from time import *
from random import uniform
from math import *
from pybot import usb4butia
from pyexported.window_setup import *
from datetime import date
from datetime import datetime
from arbolDeDecision import *
from lectorPuertosJson import *
from utils import *
import serial
import simplejson
import random
from os import system

butia = usb4butia.USB4Butia()

BOX = {}
ACTION = {}
tw = get_tw()
global_objects = None
turtles = None
canvas = None
logo = None


def start():
    logger("INICIALICE VARIABLES ! TENE EN CUENTA SENSORES==   1-ATRAS |  5-IZQUIERDA  | 6-DERECHA")
    presentacionConsola()
    print("¿Cómo se llama? ")
    #nombre = raw_input()
    nombre= " Mario "
    presentacionConsola()
    
    print('Me alegro de conocerte, el juego inciara pronto')
    
    tw.start_plugins()
    global global_objects,turtles,canvas,logo
    global_objects = tw.get_global_objects()
    logger('usuario registrado'+str(nombre))
    turtles = tw.turtles
    canvas = tw.canvas
    logo = tw.lc
    logo.boxes = BOX
    global butia
    butia = global_objects['Butia']
    turtle = turtles.get_active_turtle()
    #*************************NICIALIZACION DE VARIABLES********************************
    Tolerancia = 0.10# Variable de entorno 10% tolero los cambios
    cajaNegra()
    sensorAtras=medicionArranque(1) 
    sensorAtras=sensorAtras -sensorAtras *Tolerancia
    sensorIzquierda=medicionArranque(5)
    sensorIzquierda=sensorIzquierda-sensorIzquierda* Tolerancia
    sensorDerecha=medicionArranque(6)
    sensorDerecha=sensorDerecha -sensorDerecha *Tolerancia
    demoraRotacion= 1
    velocidadChoque= 400
    FrontalTolerancia = 25000
    #***********************************************************************************
    while True:
        Seleccion=decisionesCombate(FrontalTolerancia,butia.getGray('1', '0'),butia.getGray('5', '0'),butia.getGray('6', '0'),butia.getDistance('3', '0'),butia.getDistance('2', '0'),sensorAtras,sensorIzquierda,sensorDerecha,lecturaArduino())
        #Seleccion = -1
        loggergral(  "-------------->>>>" +str(Seleccion)+ " <----Seleccione")
        butia.speed(600.0)
        if Seleccion is 0:
            butia.left()
            butia.speed(400.0)
            butia.right()
        elif Seleccion is -1:
            butia.stop(True)

        elif  Seleccion is 1:
            butia.forward()
            sleep(0.01)
        elif Seleccion is 2:
            butia.stop(True)
            butia.speed(1000.0)
            butia.backward()
            sleep(0.5)
            butia.backward()
            butia.left()
        elif Seleccion is 3:
            butia.forward()
            sleep(demoraRotacion*0.1)
        elif Seleccion is 7:
            butia.speed(velocidadChoque)
            butia.forward()
        elif Seleccion is 8:
            butia.left()
            sleep(3)
            butia.forward()
        elif Seleccion is 4:
            h=0
            j=0
          
            while lecturaApagado() and j<3:
                butia.backward()
                sleep(demoraRotacion*0.4)
                j=j+1

            while lecturaApagado() and h<3 and butia.getDistance('3', '0')> FrontalTolerancia :
                butia.right()
                sleep(demoraRotacion*random.uniform(0.1,0.8))
                print("Girando derecha ....")
                h=h+1

        elif Seleccion is 5:
            h=0
            j=0
       
            while lecturaApagado() and j<3:
                butia.backward()
                sleep(demoraRotacion*0.4)
                j=j+1
            while lecturaApagado() and h<3 and butia.getDistance('3', '0')> FrontalTolerancia:
                butia.left()
                print("Girando Izquierda")
                sleep(demoraRotacion*random.uniform(0.1,0.8))
                h=h+1

        elif Seleccion is 15:
            butia.forward()
            sleep(demoraRotacion)
            butia.left()
#--------------------------->DECISIONES CONTROL-----------------------------------
        elif Seleccion is 30:
            butia.forward()
        elif Seleccion is 31:
            butia.right()
        elif Seleccion is 32:
            butia.backward()
        elif Seleccion is 33:
            butia.left()
        elif Seleccion is 34:
            butia.stop(True)
            presentacionConsola()
        elif Seleccion is 16:
            yield True
        cajaNegra()
            

        #yield True
    yield True





def medicionArranque(sensor):
    i=0
    lista=[]
    while i <100 :
        
        if (butia.getGray(str(sensor), '0')>0 ) :
            lista.append(butia.getGray(str(sensor), '0'))
            i = i + 1
            loggergral('agregue a la lista '+str(i) +'Variable--'+ str(butia.getGray(str(sensor), '0')))
            
        else :
            i=i
    logger ('------->'+str(sensor) + '!!!!!!!!!!!!!!!!!!!!Valor decidido'+str(min(lista)))
    return min(lista)

def cajaNegra():
    if butia.getDistance('2', '0')>0 :
            print("|FD ->.|" , butia.getDistance('3', '0'),"|AtD ->|" , butia.getDistance('2', '0'),"|At ->|",butia.getGray('1', '0') , "|I ->| ",butia.getGray('5', '0'),"|D ->|",butia.getGray('6', '0'))
            archivo = open('CajaNegraPython.txt','a')
            
            archivo.write(str(datetime.now()))
            archivo.write("|FD ->|")
            archivo.write(str(butia.getDistance('2', '0')) +' ')
            archivo.write("|AtD ->|")
            archivo.write(str(butia.getDistance('4', '0')) +' ')
            archivo.write("|At ->|")
            archivo.write(str(butia.getGray('1', '0')) +' ')
            archivo.write("|I ->|")
            archivo.write(str(butia.getGray('5', '0')) +' ')
            archivo.write("|D ->|")
            archivo.write(str(butia.getGray('6', '0')) +'\n')
            sleep(0.01)
            archivo.close()


def presentacionConsola():
    system("clear")
    print('\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n')
    print("###############################################################################")
    print("#                                                                             #")
    print("#                              SUMO PLUS V 1.0                                #")
    print("#                                                                             #")
    print("#FING-TRERB 2020                                                              #")
    print("###############################################################################")
    print('\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n'+'\n')
    

ACTION["start"] = start


if __name__ == '__main__':
    tw.lc.start_time = time()
    tw.lc.icall(start)
    gobject.idle_add(tw.lc.doevalstep)
    gtk.main()




        
