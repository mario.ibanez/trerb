#include <IRremote.h>
int receptor = 4;
int led = 13;
IRrecv irrecv(receptor);
decode_results codigo; //OBJETO CODIGO DE CLASE decode_result, oriundo de IRremote.h
 
void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn(); // INICIA LA RECEPCIÓN
  pinMode(led, OUTPUT);
}
 
void loop()
{
 if (irrecv.decode(&codigo))
 {
Serial.println(codigo.value, HEX);

      if (codigo.value==0xFF6897)//CÓDIGO DEL NÚMERO CERO PARA ACTIVAR LED
      {
         digitalWrite(led,HIGH);
      }

      if (codigo.value==0xFF30CF)//CÓDIGO DEL NÚMERO UNO PARA DESACTIVAR LED
      {
         digitalWrite(led,LOW);
      }
    
delay(100);
irrecv.resume();

 }
}
