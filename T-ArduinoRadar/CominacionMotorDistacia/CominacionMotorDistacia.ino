/*
  Capitulo 30 de Arduino desde cero en Español.
  Programa que realiza un giro completo del motor 28BYJ-48 en conjunto con el controlador
  basado en ULN2003, detiene 5 segundos y luego comienza nuevamente. La secuencia es la de
  medio paso para maxima precision y torque medio utilizando una matriz para su 
  definicion. 
  Alimentar Arduino con fuente de alimentacion externa de 6 a 12 Vdc.

  Autor: bitwiseAr  

*/

int IN1 = 12;      // pin digital 8 de Arduino a IN1 de modulo controlador
int IN2 = 11;      // pin digital 9 de Arduino a IN2 de modulo controlador
int IN3 = 10;     // pin digital 10 de Arduino a IN3 de modulo controlador
int IN4 = 9;     // pin digital 11 de Arduino a IN4 de modulo controlador
int demora = 2;      // demora entre pasos, no debe ser menor a 10 ms.
// medio paso



int TRIG = 7;      // trigger en pin 10
int ECO = 8;      // echo en pin 9
int LED = 3;      // LED en pin 3
int DURACION;
int DISTANCIA;

int paso [8][4] =   // matriz (array bidimensional) con la secuencia de pasos
{
  {1, 0, 0, 0},
  {1, 1, 0, 0},
  {0, 1, 0, 0},
  {0, 1, 1, 0},
  {0, 0, 1, 0},
  {0, 0, 1, 1},
  {0, 0, 0, 1},
  {1, 0, 0, 1}
};

int paso2 [8][4] =   // matriz (array bidimensional) con la secuencia de pasos
{
  {1, 0, 0, 1},
  {0, 0, 0, 1},
  {0, 0, 1, 1},
  {0, 0, 1, 0},
  {0, 1, 1, 0},
  {0, 1, 0, 0},
  {1, 1, 0, 0},
  {1, 0, 0, 0}
};



void setup() {
  pinMode(IN1, OUTPUT);   // todos los pines como salida
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  Serial.begin(9600); 
  pinMode(TRIG, OUTPUT);  // trigger como salida
  pinMode(ECO, INPUT);    // echo como entrada
  pinMode(LED, OUTPUT);   // LED como salida
}

void loop() {

  for (int i = 0; i < 175; i++) // 512*8 = 4096 pasos
  {
    for (int i = 0; i < 8; i++)   // bucle recorre la matriz de a una fila por vez
    {         // para obtener los valores logicos a aplicar
      digitalWrite(IN1, paso2[i][0]);  // a IN1, IN2, IN3 e IN4
      digitalWrite(IN2, paso2[i][1]);
      digitalWrite(IN3, paso2[i][2]);
      digitalWrite(IN4, paso2[i][3]);
      delay(demora);
       registro();
       
    }
   // Serial.println(i);
  }

  for (int j = 180; j >1; j--) // 512*8 = 4096 pasos
  {
    for (int j = 0; j < 8; j++)   // bucle recorre la matriz de a una fila por vez
    {         // para obtener los valores logicos a aplicar
      digitalWrite(IN1, paso[j][0]);  // a IN1, IN2, IN3 e IN4
      digitalWrite(IN2, paso[j][1]);
      digitalWrite(IN3, paso[j][2]);
      digitalWrite(IN4, paso[j][3]);
      delay(demora);
      registro();
      
    }
   // Serial.println(j); 
  }

  digitalWrite(IN1, LOW); // detiene por 5 seg.
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
  delay(1);

}



void registro()
{
  
  digitalWrite(TRIG, HIGH);     // generacion del pulso a enviar
  delay(1);       // al pin conectado al trigger
  digitalWrite(TRIG, LOW);    // del sensor
  
  DURACION = pulseIn(ECO, HIGH);  // con funcion pulseIn se espera un pulso
            // alto en Echo
  DISTANCIA = DURACION / 58.2;    // distancia medida en centimetros
  Serial.println(DISTANCIA);    // envio de valor de distancia por monitor serial
  delay(1);       // demora entre datos


   
}
