/*
  Capitulo 5 de Arduino desde cero en Español
  Segundo programa que envía mediante el Monitor Serial el valor de distancia
  leído por el sensor ultrasónico HC-SR04 y enciende y apaga LED dentro del
  rango de 0 a 20 cms.

  Autor: bitwiseAr  

*/

//***************ULTRASONIDO***********************************
int TRIG = 10;      // trigger en pin 10
int ECO = 9;      // echo en pin 9
int LED = 3;      // LED en pin 3
int DURACION;
int DISTANCIA;

//***************FOTOSENSOR*************************************

const long A = 1000;     //Resistencia en oscuridad en KΩ
const int B = 15;        //Resistencia a la luz (10 Lux) en KΩ
const int Rc = 10;       //Resistencia calibracion en KΩ
const int LDRPin = A0;   //Pin del LDR
 
int V;
int ilum;


//************** GIROSCOPIO ************************************


// Librerias I2C para controlar el mpu6050
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

// La dirección del MPU6050 puede ser 0x68 o 0x69, dependiendo 
// del estado de AD0. Si no se especifica, 0x68 estará implicito
MPU6050 sensor;

// Valores RAW (sin procesar) del acelerometro y giroscopio en los ejes x,y,z
int ax, ay, az;
int gx, gy, gz;

//**************RECEPTOR DE INFRARROJOS *************************

#include <IRremote.h>
int receptor = 4;
int ledIR = 1;
IRrecv irrecv(receptor);
decode_results codigo; //OBJETO CODIGO DE CLASE decode_result, oriundo de IRremote.h


 
 
void setup()
{
  pinMode(TRIG, OUTPUT);  // trigger como salida
  pinMode(ECO, INPUT);    // echo como entrada
  pinMode(LED, OUTPUT);   // LED como salida
  Serial.begin(115200);     // inicializacion de comunicacion serial a 9600 bps
   Wire.begin();           //Iniciando I2C  
  sensor.initialize();    //Iniciando el sensor
  irrecv.enableIRIn(); // INICIA LA RECEPCIÓN
  pinMode(ledIR, OUTPUT);
  if (sensor.testConnection()) Serial.println("Sensor iniciado correctamente");
  else Serial.println("Error al iniciar el sensor");
}

void loop()
{
  //********************************* RELEVAMIENTO DE INFO *********************************
  // Leer las aceleraciones y velocidades angulares
  sensor.getAcceleration(&ax, &ay, &az);
  sensor.getRotation(&gx, &gy, &gz);
  V = analogRead(LDRPin);         
 
   //ilum = ((long)(1024-V)*A*10)/((long)B*Rc*V);  //usar si LDR entre GND y A0 
   ilum = ((long)V*A*10)/((long)B*Rc*(1024-V));    //usar si LDR entre A0 y Vcc (como en el esquema anterior)
  
  digitalWrite(TRIG, HIGH);     // generacion del pulso a enviar
  delay(1);       // al pin conectado al trigger
  digitalWrite(TRIG, LOW);    // del sensor
  
  DURACION = pulseIn(ECO, HIGH);  // con funcion pulseIn se espera un pulso
            // alto en Echo
  DISTANCIA = DURACION / 58.2;    // distancia medida en centimetros
  Serial.print("{\"x\":"+ (String)DISTANCIA + ",\"y\":" + (String)codigo.value +",\"I\":"+(String) ilum );    // envio de valor de distancia por monitor seria
  Serial.print(",\"ax\":"+(String)ax+",\"ay\":"+ (String)ay+",\"az\":" +(String)az);
  Serial.println(",\"gx\":"+(String)ax+",\"gy\":"+ (String)ay+",\"gz\":" +(String)az + "}");


if (irrecv.decode(&codigo)){

 if ((String)codigo.value==(String)16753245)
 {digitalWrite(ledIR,HIGH);} else{digitalWrite(ledIR,LOW);}
irrecv.resume();
}
  
    // demora entre datos
  //****************************************** PINES ***************************************

  if (DISTANCIA <= 140&& DISTANCIA>0){ // si distancia entre 0 y 20 cms.
    digitalWrite(LED, HIGH);      // enciende LED
    delay(DISTANCIA *5 );      // demora proporcional a la distancia
    digitalWrite(LED, LOW);     // apaga LED
    }
  delay(150);    
}
