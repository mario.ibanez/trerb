import pyfirmata2

DELAY = 1
PORT =pyfirmata2.Arduino.AUTODETECT
board =pyfirmata2.Arduino(PORT)

board.samplingOn(100)
board.analog[4].enable_reporting()

while True:
    print(board.analog[4].read)
    board.pass_time(DELAY)

