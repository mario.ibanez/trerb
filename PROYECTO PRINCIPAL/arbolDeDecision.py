import cv2
import numpy as np	
from time import *
from utils import *

def decisionesCombate (fronTolerancia,atras,izq,der,dfron,dtras,satras,sizq,sder,listaArduino):
    loggergral(str(listaArduino))
    lim=37000
    limI=sizq 
    limD=sder 
    limA=satras 
    frente=fronTolerancia
    deatras=25000

    #********************************** CONDICIONES***********************************************
    #falla de sensores
    loggergral("|FD ->|" + str(dfron) + "|AtD ->|" + str(dtras) + "|At ->|" + str(atras) + "|I ->| "+ str(izq)+  "|D ->| "+ str(der))
    condicion0=(dfron == -1 or izq == -1 or der==-1 )
    #objeto cerca, alguno de los sensores frontales tocando  y  no los dos sobre la linea 
    condicion1=(dfron < frente and izq>sizq and der>sder)
    #frente limpio y alguno de los sensores  libre
    condicion2=(dfron > frente and  (izq>sizq and der>sder) )
    #toca linea izquierda
    condicion4=(izq<sizq)
    #toca linea derecha
    condicion5=(der<sder)
    #falla 26882
    condicion6= (izq ==26882 or  der== 26882)
    #logger("   "+str(sizq)+ "   "+ str(izq))
    #objetivo al frente
    condicion7=False
    #objetivo atras
    condicion8=(dtras<deatras)
    #pisa liena trasera
    condicion9=(atras<satras)
    #condicion control remoto
    condicion21=(str(listaArduino[1])=='16761405')
    condicion30frente=(str(listaArduino[1])=='16718055')
    condicion31derecha=(str(listaArduino[1])=='16734885')
    condicion32atras=(str(listaArduino[1])=='16730805')
    condicion33izquierda=(str(listaArduino[1])=='16716015')
    condicion34stop=(str(listaArduino[1])=='16726215')


    #*********************************DECISIONES***************************************************
    if condicion0 == True :
        print("---------------------------->Falla Sensores Python-------------------------"+ '\n')
        sleep(0.1)
        return 0
    if condicion30frente == True:
        logger("CONTROL=  FRENTE"+ '\n')
        return 30
        
    elif condicion31derecha == True:
        logger("CONTROL=  DERECHA"+ '\n')
        return 31
    elif condicion32atras== True:
        logger("CONTROL=  ATRAS"+ '\n')
        return 32
    elif condicion33izquierda== True:
        logger("CONTROL=  IZQUIERDA"+ '\n')
        return 33
    elif condicion34stop== True:
        logger("CONTROL=  ALTO"+ '\n')
        return 34


    elif condicion21 == False:
        return -1
    elif condicion1 == True :
        loggergral("---------------------------->Entre Condicion 1-------------------------"+ '\n')
        return 1
    elif condicion6 == True :
        logger("###################-----> FALLA EN SENSORES <------#####################")
        logger26882(" I-> "+ str(izq)+"D-> "+ str(der)+"T->"+ str(atras))
        return 6
    elif condicion9 == True :
        return 1
    elif condicion7 == True :
        loggergral("----->Entre Condicion 7 ATAQUE!!!!!! -------"+ '\n')
        return 7
    elif condicion8 == True :
        loggergral("#######Entre Condicion 8 (VER SENSOR DESCOMPUESTO DE ATRAS)-----------"+ '\n')
        return 8 
    elif condicion2 == True :
        loggergral("------>Entre Condicion 3 ADELANTE --"+ '\n')
        return 3
    elif condicion4 == True :
        loggergral("----->Entre Condicion 4 GIRAR IZQUIERDA-----"+ '\n')
        return 4
        
    elif condicion5 == True :
        loggergral("----->Entre Condicion 5 GIRAR DERECHA ------"+ '\n')     
        return 5

    elif condicion1 == False:
        logger("------->Entre Condicion 15 NO HACEMOS NADAAA-----"+ '\n')
        return 15


