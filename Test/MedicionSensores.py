#!/usr/bin/env python
# -*- coding: utf-8 -*-

_INSTALL_PATH = '/usr/share/sugar/activities/TurtleArt.activity'
_ALTERNATIVE_INSTALL_PATH = \
    '/usr/local/share/sugar/activities/TurtleArt.activity'
import numpy
import os, sys, dbus
from matplotlib import pyplot as plt

paths = []

paths.append('../%s.activity')
paths.append(os.path.expanduser('~') + '/Activities/%s.activity')
paths.append('/usr/share/sugar/activities/%s.activity')
paths.append('/usr/local/share/sugar/activities/%s.activity')
paths.append(
    '/home/broot/sugar-build/build/install/share/sugar/activities/%s.activity')
paths.append('/usr/share/sugar/activities/TurtleBots.activity')
flag = False
for path in paths:
    for activity in ['TurtleBots', 'TurtleBlocks']:
        p = (path % activity) if "%" in path else path

        if os.path.exists(p) and p not in sys.path:
            flag = True
            sys.path.insert(0, p)

if not flag:
    print 'This code require the Turtle Blocks/Bots activity to be installed.'
    exit(1)

from time import *
from random import uniform
from math import *

#from pyexported.window_setup import *


tw = get_tw()

BOX = {}
ACTION = {}

global_objects = None
turtles = None
canvas = None
logo = None

def start():
    while True:
        tw.start_plugins()
        global global_objects,turtles,canvas,logo
        global_objects = tw.get_global_objects()
        turtles = tw.turtles
        canvas = tw.canvas
        logo = tw.lc
        logo.boxes = BOX
        global butia
        butia = global_objects['Butia']
        turtle = turtles.get_active_turtle()
        logo.show(u'SensorGris', True)
        print("SensorGris",butia.getGray('5', '0'))
        tw.clear_plugins()
        logo.stop_playing_media()
        logo.reset_scale()
        logo.clear_value_blocks()
        canvas.clearscreen()
        logo.reset_internals()
        turtles.reset_turtles()
        turtle = turtles.get_active_turtle()
        logo.show(butia.getGray('5', '0'), True)
        logo.show(u'Frontal', True)
        tw.clear_plugins()
        logo.stop_playing_media()
        logo.reset_scale()
        logo.clear_value_blocks()
        canvas.clearscreen()
        logo.reset_internals()
        turtles.reset_turtles()
        turtle = turtles.get_active_turtle()
        print("Frontal |",butia.getDistance('4', '0'))
        logo.show(butia.getDistance('4', '0'), True)
        logo.show(u'Inferior', True)
        tw.clear_plugins()
        logo.stop_playing_media()
        logo.reset_scale()
        logo.clear_value_blocks()
        canvas.clearscreen()
        logo.reset_internals()
        turtles.reset_turtles()
        turtle = turtles.get_active_turtle()
        logo.show(butia.getDistance('1', '0'), True)
        print("Inferior",butia.getDistance('1', '0'))
        archivo = open('hello.txt','a')
        archivo.write("Frontal |")
        archivo.write(str(butia.getDistance('4', '0')) +'\n')
        archivo.write("SensorGris |")
        archivo.write(str(butia.getGray('5', '0')) +'\n')
        archivo.write("Inferior |")
        archivo.write(str(butia.getDistance('1', '0')) +'\n')
        archivo.close()
        plt.ion()

        plt.plot(butia.getDistance('1', '0'),9)
       

        yield True
ACTION["start"] = start


if __name__ == '__main__':
    tw.lc.start_time = time()
    tw.lc.icall(start)
    gobject.idle_add(tw.lc.doevalstep)
    gtk.main()
