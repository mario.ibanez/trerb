#!/usr/bin/env python
# -*- coding: utf-8 -*-

_INSTALL_PATH = '/usr/share/sugar/activities/TurtleArt.activity'
_ALTERNATIVE_INSTALL_PATH = \
    '/usr/local/share/sugar/activities/TurtleArt.activity'

import os, sys, dbus
from matplotlib import pyplot as plt
import math
import numpy as np
sys.path.insert(0, '/usr/share/sugar/activities/TurtleBots.activity/plugins/butia')

paths = []

paths.append('../%s.activity')
paths.append(os.path.expanduser('~') + '/Activities/%s.activity')
paths.append('/usr/share/sugar/activities/%s.activity')
paths.append('/usr/local/share/sugar/activities/%s.activity')
paths.append(
    '/home/broot/sugar-build/build/install/share/sugar/activities/%s.activity')
paths.append('/usr/share/sugar/activities/TurtleBots.activity')
flag = False
for path in paths:
    for activity in ['TurtleBots', 'TurtleBlocks']:
        p = (path % activity) if "%" in path else path

        if os.path.exists(p) and p not in sys.path:
            flag = True
            sys.path.insert(0, p)

if not flag:
    print 'This code require the Turtle Blocks/Bots activity to be installed.'
    exit(1)

from time import *
from random import uniform
from math import *
from pybot import usb4butia
import time

robot = usb4butia.USB4Butia()

BOX = {}
ACTION = {}

global_objects = None
turtles = None
canvas = None
logo = None

while True:

        print("|Atras|",robot.getGray('1', '0') , "|Distancia|" , robot.getDistance('2', '0'),"|Izquierdo| ",robot.getGray('5', '0'),"|Derecho|",robot.getGray('6', '0'))
        archivo = open('hello.txt','a')
        archivo.write("|Distancia|")
        archivo.write(str(robot.getDistance('2', '0')) +' ')
        archivo.write("|Atras|")
        archivo.write(str(robot.getGray('1', '0')) +' ')
        
        archivo.write("|Izquierdo|")
        archivo.write(str(robot.getGray('5', '0')) +' ')
        archivo.write("|Derecho|")
        archivo.write(str(robot.getGray('6', '0')) +'\n')
        archivo.close()
        sleep(1)
        x = np.array(range(500))*0.1
        y = np.zeros(len(x))
        for i in range(len(x)):
            y[i] = math.sin(x[i])
        plt.ion()

        plt.plot(robot.getDistance('1', '0'),9)
       