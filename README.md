# TRERB

Proyecto de robot  sumo con Butía para la materia Taller de Robotica con el Robot Butía. Facultad de Ingeniería - Universidad de la República. 
Mario Andres Ibáñez Grajales  /*48008660*/

Implementación Progresiva de Luchador de Sumo

Motivación 

Los robots de Sumo son desarrollos bastante accesibles para personas que se empiezan a incorporar al tema de la robótica es un acercamiento muy
interesante para los jóvenes, además de que al poderse competir permite que las habilidades se vallan perfeccionando y uno puede darle tanto en
calidad de software como de hardware que uno quiera.  Posiblemente sea de las plataformas más entretenidas para los niños y algunos adultos, ya
que les da la posibilidad de comparar su desarrollo directamente con otros e irlo perfeccionarlo con la experiencia.
La elección del tema surge de una la tormenta de ideas sobre muchos proyectos no prosperaron porque en general eran demasiado ambiciosos, requerí-
an 
inversiones extra importantes, desarrollos especiales como también otros que eran demasiado simples y se los descarto.
De la pre-Selección surgió también el tema de un seguidor de líneas en dos etapas, la primera que solo cumpliera con las funciones básicas y la 
segunda en la que pudiera cargar/mover elementos sobre la línea, deteniéndose a intervalos o en lugares. Sin embargo, se descarto en favor del Sumo,
que es un  proyecto menos ambicioso, pero más práctico, fácil de implementar y escalable.
Por más información sobre el sumo robot: https://en.wikipedia.org/wiki/Robot-sumo
Cuando se analizó la elección del proyecto se basó en una tormenta de ideas, como se mencionó, siempre se buscó desarrollar algo simple que estuvi-
era bien documentado y que se pudiera implementar por aficionados y jóvenes, pero a su vez que se pudiera escalar con relativa facilidad, bien sea 
para enriquecer el material que se genere o bien sea para procurar generar más interés en el usuario que quiera profundizar en el tema.
Otra de las premisas con las que se quiso trabajar es aprovechar al máximo el kit de sensores que nos proporciona butiá, por lo cual los 4 sensores
de grises y los dos sensores de distancia proporcionan herramientas más que útiles para el desarrollo de este tipo de proyectos.
Además, queremos tener en cuenta que butiá nos proporcionó siempre la capacidad de una plataforma móvil, mediante la cual nuestro robot se podía 
desplazar por la superficie, cosa que se quiso añadir a este desarrollo para aprovechar esta característica, sumado a la carcasa de acrílico que 
cuenta con cierta resistencia y que en realidad para proyectos no profesionales y competiciones infantiles o de adolescentes son más que aceptables
 y se puede hacer con un costo cero para estudiantes/docentes.

Cabe recordar que cuando se presento la tarea 2, el costo de un prototipo se valuó en aproximadamente $U 3.210,00, lo que si bien no lo hacia inaccesi-
ble si generaba un escollo importante para el implementarlo en un aula.
Cabe aclarar que elegimos el titulo de progresiva porque lo que se procura es que el usuario valla progresando e incorporando nuevos conocimientos, 
el proyecto no es algo que se acabe en si mismo, sino la plataforma sobre la cual mejorar, teniendo siempre presente también la premisa de “no re-
inventar la pólvora”  y aprovechar todo el conocimiento y los diseños que se nos brinda, tanto de la plataforma Butiá como de otras plataformas educa-
tivas. 


Materiales

Para el siguiente proyecto, el cual se ha divide dos etapas
Se van a necesitar los componentes del kit robótico educativo, esto es 
•	El robot Butiá 2.0
•	2-4 sensores de Grises
•	Sensor de Botón
•	Tornillos Plásticos
•	Cables Sensores
•	Cable USB
•	Pilas
•	Motores X 2

Público Objetivo

Se define un proyecto en dos etapas, la etapa básica/primaria o completa en la cual se parte de un conocimiento desde 0 y la etapa complementaria/final 
donde se expone otro tipo de información que lo  que busca es ayudar  a atraer a los  estudiantes y jóvenes que se consideren atraídos por el contenido
del tutorial, esta última parte es más densa, por lo cual se dejó para una segunda parte, a fin del que el usuario  se sienta atraído en la primera parte 
y concrete su interés en la segunda.
Para la fase básica del proyecto: niños de entre 10-12 años, con algún conocimiento de la robótica y gusto por la electrónica.
En la etapa primaria los objetivos a conseguir son
•	Que el robot no se salga de la pista.
•	Que el robot escanee el área en busca del rival.
•	Que el robot empuje al rival hasta sacarlo de área de la competencia.
Estos tres objetivos son los más básicos que debe cumplir el robot de sumo para poder cumplir su tarea. En sí, al estar ya diseñada la plataforma que
lo soporta, la ubicación de los sensores y la programación son tareas fácilmente realizables por niños.
Para la etapa complementaria, dirigida a adolescentes de 14-16 años con gusto por la robótica.
•	incorporación de sensores extra y mejoras en el diseño 
•	mejoras en el rendimiento del robot 
•	desarrollos de estrategias básicas y de complejidad media 
•	incorporación de extras diversos. 
Cabe destacar que después del cumplimiento de la primera etapa, en la cual los usuarios tendrán un robot de sumo autónomo, nos dedicaremos a poder 
englobar ciertos conocimientos que resultan complementarios y de cierta forma son un exceso para los usuarios cuyo interés es solo divertirse. Se
 incorporarán material audiovisual que se considere pertinente y documentación que permita a los usuarios poder expandir su conocimiento.

